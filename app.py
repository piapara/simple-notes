from flask import Flask, render_template
from dao import folder_dao
from model.folder import Folder

app = Flask(__name__)


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/folder')
def list_folders():
    return 'Listing folders...'


@app.route('/folder/new/<folder_name>/<folder_path>')
def add_folder(folder_name: str, folder_path: str):
    f = folder_dao.FolderDAO()
    f.save(Folder(name=folder_name, path=folder_path))
    return ''


@app.route('/notes')
def list_notes():
    return 'Listing notes'


@app.route('/notes/new', methods=["POST"])
def add_note():
    pass


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5050, debug=True)
