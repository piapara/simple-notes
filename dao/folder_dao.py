from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from model.base import Base
from model.folder import Folder

engine = create_engine("postgresql+psycopg2://notes_admin:ba!test123@localhost:54320/notes")


class FolderDAO:

    def __init__(self):
        self.session = sessionmaker()
        self.session.configure(bind=engine)
        Base.metadata.create_all(engine)

    def save(self, folder: Folder):
        s = self.session()
        s.add(folder)
        s.commit()
