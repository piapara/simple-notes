from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from model.base import Base
from model.note import Notes

engine = create_engine("postgresql+psycopg2://notes_admin:ba!test123@localhost:54320/notes")


class NoteDAO:

    def __init__(self):
        self.session = sessionmaker()
        self.session.configure(bind=engine)
        Base.metadata.create_all(engine)

    def save(self, note: Notes):
        s = self.session()
        s.add(note)
        s.commit()
