from sqlalchemy import Column, Integer, String
from model.base import Base


class Folder(Base):
    __tablename__ = 'folder'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    path = Column(String, unique=True)
    user = Column(String)
