from sqlalchemy import Column, Integer, String, ForeignKey
from model.base import Base


class Notes(Base):
    __tablename__ = 'notes'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    content = Column(String)
    folder_id = Column(Integer, ForeignKey('folder.id'))
